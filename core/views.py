# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate, login
from .forms import RegisterForm
from django.contrib.auth.decorators import login_required


# Create your views here.

def index(request):
	# documents = Document.objects.all()
	template = loader.get_template('cliente/index.html')
	return HttpResponse(template.render())

def renderSignup(request):
	template = loader.get_template('cliente/signup.html')
	form = RegisterForm()
	context = {
		'form' : form
	}
	return render(template, context)

def renderSignin(request):
	template = loader.get_template('cliente/signin.html')
	return HttpResponse(template.render())

def register(request):
	template = 'cliente/register.html'
	form = RegisterForm(request.POST)
	if form.is_valid():
		user = form.save()
		user = authenticate(
		username=user.username, password=form.cleaned_data['password1']
		)
		login(request, user)
		return redirect('index')
	else:
		form = RegisterForm()
		context = {
			'form' : form
		}
	return render(request, template, context)

@login_required
def painel(request):
	template_name = 'cliente/painel.html'
	return render(request, template_name)

@login_required
def logout(request):
	logout()
	return redirect('home')
