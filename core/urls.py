from django.conf.urls import url
from django.contrib.auth.views import login, logout
from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^register/$', views.register, name='register'),
	url(r'^login$', login,{'template_name': 'cliente/signin.html'}, name='login'),
	url(r'^logout$', logout, name='logout'),
	url(r'^painel$', views.painel, name='painel'),
]
