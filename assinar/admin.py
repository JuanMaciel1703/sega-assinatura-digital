# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from assinar.models import Document, User



# Register your models here.
admin.site.register(Document)
admin.site.register(User)
