from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.decorators import login_required
from . import sign

from .models import Document
from .forms import DocumentForm

@login_required
def home(request):
    documents = Document.objects.filter(user=request.user)
    return render(request, 'home.html', { 'documents': documents })

@login_required
def simple_upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        return render(request, 'simple_upload.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'simple_upload.html')

@login_required
def model_form_upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            print(request.POST)
            print(request.FILES)
            if not (sign.main(request.POST.get("publicKey", ""), request.POST.get("assinatura", ""), request.FILES.get("document", ""))):
                print('ASSINATURA VALIDA')
                tmp = form.save(commit=False)
                tmp.user = request.user
                tmp.save()
                return redirect('home')
            else:
                print('ASSINATURA INVALIDA')
                return redirect('model_form_upload')
    else:
        form = DocumentForm()
    return render(request, 'model_form_upload.html', {
        'form': form
    })
