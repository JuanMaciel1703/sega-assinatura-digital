function LerArquivo(file, done, fail) {
    var reader = new FileReader;
    reader.onload = function (evt) {
        done(evt.target.result);
    };
    reader.onerror = function (evt) {
        fail();
    };
    reader.readAsText(file);
}

var txtPublic = document.getElementById("public");
var txtAssi = document.getElementById("assinatura");
var txtHash = document.getElementById("hash");

document.getElementById("id_document").onchange = function () {
    if (this.files.length === 1) {
        LerArquivo(this.files[0], function (conteudo) {
            var hash = SHA256(conteudo);
            txtHash.value = hash;
            document.getElementById("status").innerHTML = "ARQUIVO LIDO!";
            document.getElementById("ass").removeAttribute("disabled");
        }, function () {
            response.value = "Falha ao ler o arquivo";
        });
    }
};

function mostrarSelect() {
    document.getElementById("selectKey").removeAttribute("hidden");
}

document.getElementById("privateKey").onchange = function () {
    if (this.files.length === 1) {
        LerArquivo(this.files[0], function (conteudo) {
            var crypt = new JSEncrypt();
            crypt.setKey(conteudo);
            var publicKey = crypt.getPublicKey();
            var assinatura = cryptico.encrypt(txtHash.value, conteudo);

            txtAssi.value = assinatura.cipher;
            txtPublic.value = publicKey;

        }, function () {
            response.value = "Falha ao ler o arquivo";
        });
    }
};
