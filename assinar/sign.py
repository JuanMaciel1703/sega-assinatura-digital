import os

from OpenSSL import crypto
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import MD5

def verificar(public_key_loc, signature, data):
    rsakey = RSA.importKey(public_key_loc)
    signer = PKCS1_v1_5.new(rsakey)
    digest = MD5.new()
    digest.update(data.read())
    if signer.verify(digest, signature):
        return True
    return False

def main(chave, assinatura, doc):
    print "Verficando Assinatura"
    print verificar(chave, assinatura, doc)
