from django import forms

from assinar.models import Document


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('__all__')
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(DocumentForm, self).__init__(*args,**kwargs)
        self.fields['description'].widget.attrs['class'] = 'form-control'
        self.fields['document'].widget.attrs['class'] = 'form-control'
        self.fields['document'].widget.attrs['id'] = 'arquivo'
